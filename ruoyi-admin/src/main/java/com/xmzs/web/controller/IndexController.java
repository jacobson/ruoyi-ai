package com.xmzs.web.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import com.xmzs.common.core.config.RuoYiConfig;
import com.xmzs.common.core.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页
 *
 * @author Lion Li
 */
@SaIgnore
@RequiredArgsConstructor
@Controller
public class IndexController {


    /**
     * 访问首页，提示语
     */
    @GetMapping("/")
    public String index() {
        return "index.html";
    }
}
