package com.xmzs.common.chat.domain.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

/**
 * 描述：
 *
 * @author https:www.unfbx.com
 * @sine 2023-04-08
 */
@Data
public class ChatRequest {
    @NotEmpty(message = "传入的模型不能为空")

    private String model;

    @NotEmpty(message = "提示词不能为空")
    private String prompt;

    @NotEmpty(message = "对话ID不能为空")
    private String conversationId;

    private String userId;

    /**
     * gpt的默认设置
     */
    private String systemMessage =  "You are ChatGPT, a large language model trained by OpenAI. Follow the user's instructions carefully. Respond using markdown.";

    private double top_p = 1;

    private double temperature = 0.2;

    /**
     * 上下文的条数
     */
    private Integer contentNumber = 10;

    /**
     * 是否携带上下文
     */
    private Boolean usingContext = Boolean.FALSE;

}
