package com.xmzs.common.chat.service.impl;


import com.xmzs.common.chat.service.ChatService;
import com.xmzs.common.chat.domain.request.ChatProcessRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;



/**
 * @author hncboy
 * @date 2023/3/22 19:41
 * 聊天相关业务实现类
 */
@Slf4j
@Service
public class ChatServiceImpl implements ChatService {

    @Override
    public ResponseBodyEmitter chatProcess(ChatProcessRequest chatProcessRequest) {
        return new ResponseBodyEmitter(0L);
    }
}
