package com.xmzs.common.chat.service;


import com.xmzs.common.chat.domain.request.ChatRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

/**
 * 描述：
 *
 * @author https:www.unfbx.com
 * @date 2023-04-08
 */
public interface SseService {

    /**
     * 客户端发送消息到服务端
     * @param chatRequest
     */
    ResponseBodyEmitter sseChat(ChatRequest chatRequest);
}
