package com.xmzs.common.demo;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import com.xmzs.common.response.PayResponse;
import com.xmzs.common.service.PayService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * 测试
 *
 * @author: wangle
 * @date: 2023/7/3
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/pay")
public class PayTest {
    private final PayService payService;

    /**
     * 获取支付二维码
     *
     * @Date 2023/7/3
     * @param response
     * @return void
     **/
    @GetMapping("/payUrl")
    public void payUrl(HttpServletResponse response){
        String payUrl = payService.getPayUrl("20160806151343349", "测试", 0.2, "192.168.2.10");
        System.out.println(payUrl);
        try {
            QrConfig config = new QrConfig(300, 300);
            QrCodeUtil.generate(payUrl, config, "png", response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转通知地址
     *
     * @Date 2023/7/3
     * @param
     * @return void
     **/
    @PostMapping("/notifyUrl")
    public void notifyUrl() {
        System.out.println("notifyUrl===========");
    }

    /**
     * 跳转通知地址
     *
     * @Date 2023/7/3
     * @param payResponse
     * @return void
     **/
    @PostMapping("/returnUrl")
    public void returnUrl(PayResponse payResponse) {
        System.out.println("商户订单号："+payResponse.getOutTradeNo());
    }

}
