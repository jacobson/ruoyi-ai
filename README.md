## 平台简介
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitee.com/dromara/RuoYi-Vue-Plus/blob/master/LICENSE)
[![使用IntelliJ IDEA开发维护](https://img.shields.io/badge/IntelliJ%20IDEA-提供支持-blue.svg)](https://www.jetbrains.com/?from=RuoYi-Vue-Plus)
<br>
[![RuoYi-Vue-Plus](https://img.shields.io/badge/RuoYi_Vue_Plus-5.0.0-success.svg)](https://gitee.com/dromara/RuoYi-Vue-Plus)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-3.0-blue.svg)]()
[![JDK-17](https://img.shields.io/badge/JDK-17-green.svg)]()

> 基于ruoyi-plus实现AI聊天和绘画功能-后端

> 本项目完全开源免费！
后台管理界面使用elementUI服务端使用Java17+SpringBoot3.X

实现功能
1. 微信登录
2. 消息记录
3. GPT-3.5、GPT-4.0对话模型
4. 敏感词过滤

>项目地址
<ul>
<li>后端: https://gitee.com/ageerle/ruoyi-ai</li>
<li>前端: https://gitee.com/ageerle/ruoyi-web</li>
<li>小程序端: https://gitee.com/ageerle/ruoyi-uniapp</li>
<li>管理端: https://gitee.com/ageerle/ruoyi-admin</li>
<li>演示地址: web.pandarobot.chat </li>
</ul>


## PC端演示

<div>
  <img style="margin-top:10px" src="./image/07.png" alt="drawing" width="550px" height="300px"/>
  <img style="margin-top:10px" src="./image/08.png" alt="drawing" width="550px" height="300px"/>
</div>

## 进群学习
<div>
  <img src="./image/01.png" alt="drawing" width="300px" height="300px"/>
</div>



## 参考项目
<ol>
<li>https://github.com/Grt1228/chatgpt-java</li>
<li>https://gitee.com/dromara/RuoYi-Vue-Plus</li>
<li>https://github.com/hncboy/ai-beehive</li>
</ol>
