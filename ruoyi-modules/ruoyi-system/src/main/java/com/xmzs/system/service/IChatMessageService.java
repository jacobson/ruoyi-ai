package com.xmzs.system.service;

import com.xmzs.system.domain.vo.ChatMessageVo;
import com.xmzs.system.domain.bo.ChatMessageBo;
import com.xmzs.common.mybatis.core.page.TableDataInfo;
import com.xmzs.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 用户对话记录Service接口
 *
 * @author Lion Li
 * @date 2023-05-19
 */
public interface IChatMessageService {

    /**
     * 查询用户对话记录
     */
    ChatMessageVo queryById(Long id);

    /**
     * 查询用户对话记录列表
     */
    TableDataInfo<ChatMessageVo> queryPageList(ChatMessageBo bo, PageQuery pageQuery);

    /**
     * 查询用户对话记录列表
     */
    List<ChatMessageVo> queryList(ChatMessageBo bo);

    /**
     * 新增用户对话记录
     */
    Boolean insertByBo(ChatMessageBo bo);

    /**
     * 修改用户对话记录
     */
    Boolean updateByBo(ChatMessageBo bo);

    /**
     * 校验并批量删除用户对话记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
