package com.xmzs.system.domain.bo;

import com.xmzs.system.domain.ChatMessage;
import com.xmzs.common.mybatis.core.domain.BaseEntity;
import com.xmzs.common.core.validate.AddGroup;
import com.xmzs.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 用户对话记录业务对象 chat_message
 *
 * @author Lion Li
 * @date 2023-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = ChatMessage.class, reverseConvertGenerate = false)
public class ChatMessageBo extends BaseEntity {

    /**
     * id
     */
    private Long id;

    /**
     * 消息内容
     */
    @NotBlank(message = "消息内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 消息发送方
     */
    @NotBlank(message = "消息发送方不能为空", groups = { AddGroup.class, EditGroup.class })
    private String role;

    /**
     * 消息接收方
     */
    @NotBlank(message = "消息接收方不能为空", groups = { AddGroup.class, EditGroup.class })
    private String userId;

    /**
     * 备注
     */
    private String remark;


}
