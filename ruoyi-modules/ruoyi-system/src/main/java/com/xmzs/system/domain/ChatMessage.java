package com.xmzs.system.domain;

import com.xmzs.common.mybatis.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 用户对话记录对象 chat_message
 *
 * @author Lion Li
 * @date 2023-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("chat_message")
public class ChatMessage extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息发送方
     */
    private String role;

    /**
     * 消息接收方
     */
    private String userId;

    /**
     * 备注
     */
    private String remark;


}
