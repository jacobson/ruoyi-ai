package com.xmzs.system.mapper;

import com.xmzs.system.domain.ChatMessage;
import com.xmzs.system.domain.vo.ChatMessageVo;
import com.xmzs.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 用户对话记录Mapper接口
 *
 * @author Lion Li
 * @date 2023-05-19
 */
public interface ChatMessageMapper extends BaseMapperPlus<ChatMessage, ChatMessageVo> {

}
